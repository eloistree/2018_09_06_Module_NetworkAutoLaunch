﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NetworkAutoLauncherMono : MonoBehaviour {



    public string m_userNameDefault = "Unnamed";
    public string m_roomNameDefault = "Default";
    public string m_passwordDefault = "Admin";
    public NetworkAutoLauncher.ConnectionType m_connectionDefault = NetworkAutoLauncher.ConnectionType.UNET_LAN;
    public GameObject m_userPrefabDefault;

    public int m_portDefault = 7777;

    [Header("Commun")]
    public string m_resourcesScenePath;
    public string m_resourcesScene;

    [Header("UNet Lan")]
    public string m_lanScenePath;
    public string m_lanScene;


    [Header("UNet Lobby")]
    public string m_lobbyScenePath;
    public string m_lobbyScene;


    [Header("Conclusion")]
    public string m_offlineScenePath;
    public string m_offlineScene;
    public string m_onlineScenePath;
    public string m_onlineScene;

}
