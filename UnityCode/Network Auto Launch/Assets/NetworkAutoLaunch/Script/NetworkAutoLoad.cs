﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

public class NetworkAutoLoad :  NetworkDiscovery
{

    private NetworkManager m_networkManager;
    private NetworkLobbyManager m_lobbyManager;
    public float m_timeBeforeAutoLaunchingRoom = 10;
    

    private void OnLevelWasLoaded(int level)
    {
        Debug.Log("Level Loaded: " + SceneManager.GetActiveScene().name);

    }

    public IEnumerator AutoJoinsHostOrBecomeClient()
    {
            if (m_lobbyManager == null)
                m_lobbyManager = FindObjectOfType<NetworkLobbyManager>();
            if (m_lobbyManager != null)
            {
                m_lobbyManager.StartMatchMaker();
            yield return new WaitForSeconds(0.1f);
                FindInternetMatch(DEFAULT_MATCH_NAME);
            yield return new WaitForSeconds(0.1f);
            yield return ReadyToPlayCoroutine();
            }
    } //this method is called when your request for creating a match is returned

    private IEnumerator ReadyToPlayCoroutine()
    {
      //  while (true)
        {
            yield return new WaitForSeconds(m_timeBeforeAutoLaunchingRoom);
            ReadyToPlay();
        }
    }

    private void OnInternetMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            //Debug.Log("Create match succeeded");

            MatchInfo hostInfo = matchInfo;
            NetworkServer.Listen(hostInfo, 9000);

            NetworkManager.singleton.StartHost(hostInfo);
        }
        else
        {
            Debug.LogError("Create match failed");
        }
    }

    public static string DEFAULT_MATCH_NAME = "HeyMonAmi";
    //call this method to find a match through the matchmaker
    public void FindInternetMatch(string matchName)
    {
        NetworkManager.singleton.matchMaker.ListMatches(0, 10, matchName, true, 0, 0, OnInternetMatchList);
    }

    //this method is called when a list of matches is returned
    private void OnInternetMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            if (matches.Count != 0)
            {
                Debug.Log("Match Found");

                //join the last server (just in case there are two...)

                NetworkManager.singleton.matchMaker.JoinMatch(matches[matches.Count - 1].networkId, "", "", "", 0, 0, OnJoinInternetMatch);
            }
            else
            {

                Debug.Log("Creat Match");
                CreateInternetMatch(DEFAULT_MATCH_NAME);
            }
        }
        else
        {
            Debug.LogError("Couldn't connect to match maker");
        }
    }

    //this method is called when your request to join a match is returned
    private void OnJoinInternetMatch(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            //Debug.Log("Able to join a match");

            MatchInfo hostInfo = matchInfo;
            NetworkManager.singleton.StartClient(hostInfo);

        }
        else
        {
            Debug.LogError("Join match failed");
        }
    }

    private static void ReadyToPlay()
    {
        Debug.Log("Hey hey");
        foreach (var player in FindObjectsOfType<NetworkLobbyPlayer>())
        {
            player.SendReadyToBeginMessage();
        }
            
            
    }
    //private static bool m_isRunning;
    //internal static bool IsRunning()
    //{
    //    return m_isRunning;
    //}

    //call this method to request a match to be created on the server
    public void CreateInternetMatch(string matchName)
    {
        NetworkManager.singleton.StartMatchMaker();
        NetworkManager
            .singleton.matchMaker
            .CreateMatch(
            matchName, 4, true, "", "", "", 0, 0, 
            OnInternetMatchCreate);
    }



    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        base.OnReceivedBroadcast(fromAddress, data);

        Debug.Log("Receibed Boardcast " + fromAddress + "  " + data);
        if (data.IndexOf("Server")>=0)
        {
            StartCoroutine(Server(fromAddress));

        }
    }

    public IEnumerator Server(string address) {
        yield return new WaitForEndOfFrame();
        m_networkManager.networkAddress = address.Substring(address.LastIndexOf(':') + 1);
        StopBroadcast();
        StopAllCoroutines();

        m_networkManager.StopServer();
        m_networkManager.StartClient();
        Destroy(this);
    }
    private string m_serverAddressReceived;

    public float checkForServer = 5;
    public UnityEvent m_onServerNotFound;
    public UnityEvent m_onServerFound;

    private void Start()
    {


        StartCoroutine(AutoJoinsHostOrBecomeClient());

        if (m_lobbyManager == null)
            m_lobbyManager = FindObjectOfType<NetworkLobbyManager>();
        if (m_networkManager == null)
            m_networkManager = FindObjectOfType<NetworkManager>();
        if (m_networkManager != null && m_lobbyManager==null)
        {

            broadcastData = "Player";
            m_serverAddressReceived = "";
            base.Initialize();
            base.StartAsClient();
            Invoke("LaunchServerIfNotFound", checkForServer);
        }


    }
    public void LaunchServerIfNotFound()
    {
        if (m_serverAddressReceived == "")
        {
            StartBoardcast();

            m_networkManager.StartHost();
            Debug.Log("Become Server ! ");

        }

    }


    public void StartBoardcast()
    {
        StopBroadcast();
        broadcastData = "Server";
        base.Initialize();
        base.StartAsServer();

    }

    public void OnDestroy()
    {
        
    }
}