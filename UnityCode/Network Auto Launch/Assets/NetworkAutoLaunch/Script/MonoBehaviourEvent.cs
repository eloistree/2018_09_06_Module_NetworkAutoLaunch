﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonoBehaviourEvent : MonoBehaviour {

    public UnityEvent m_awake;
    public UnityEvent m_start;
    public UnityEvent m_enable;
    public UnityEvent m_disable;
    public UnityEvent m_destroy;

    private void Awake()
    {

        m_awake.Invoke();
    }
    
    void Start () {

        m_start.Invoke();
    }
    private void OnEnable()
    {

        m_enable.Invoke();
    }
    private void OnDisable()
    {
        m_disable.Invoke();
    }
    
    private void OnDestroy()
    {
        m_destroy.Invoke();

    }
}
