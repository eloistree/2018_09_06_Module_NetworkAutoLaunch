﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class NetworkAutoLauncher {

    private static NetworkAutoLauncherMono m_info;
    public static NetworkAutoLauncherMono Singleton { get {
            if (m_info == null)
            {
                m_info = GameObject.FindObjectOfType<NetworkAutoLauncherMono>();
            }
            if (m_info == null)
            {
               GameObject obj = Resources.Load<GameObject>("NetworkAutoLauncher");
               obj =  GameObject.Instantiate(obj);
                obj.name = "#NetworkAutoLaunch";
                m_info = obj.GetComponent<NetworkAutoLauncherMono>();
                if (m_info == null)
                    GameObject.Destroy(obj);
            }
            return m_info;
        }
    }
    public static string GetUserName()
    {
        if (string.IsNullOrEmpty(m_wantedUserName))
            return Singleton.m_roomNameDefault;
        return m_wantedUserName;
    }
    public static string GetRoomName()
    {
        if (string.IsNullOrEmpty(m_wantedRoomName))
            return Singleton.m_roomNameDefault;
        return m_wantedRoomName;
    }
    public static int GetServerPort()
    {
        if (m_port<=0)
            return Singleton.m_portDefault;
        return m_port;
    }
    public static string GetPasswordName() {
        if (string.IsNullOrEmpty(m_wantedPasswordName))
            return Singleton.m_passwordDefault;
        return m_wantedPasswordName;
    }
    public static GameObject GetUserPrefab() { return Singleton.m_userPrefabDefault; }




    public static string m_wantedUserName;
    public static string m_wantedPasswordName;
    public static string m_wantedRoomName;
    public static ConnectionType m_wantedConnection;
    public static int m_port;

    internal static void SetConnectionType(ConnectionType type)
    {
        m_wantedConnection = type;
    }
    internal static void SetUserName(string text)
    {
        m_wantedUserName = text;
    }

    internal static void SetRoomName(string text)
    {
        m_wantedRoomName = text;
    }

    internal static void SetRoomName(int port)
    {
        m_port = port;
    }
    public static void ConnectToUNetLobby()
    {
        ConnectToScene(Singleton.m_lobbyScene);
    }


    public static void ConnectToUNetLan()
    {
        ConnectToScene(Singleton.m_lanScene);
    }

    private static void ConnectToScene(string name)
    {
        
        KillAllNetworkManager();
        SceneManager.activeSceneChanged += ApplyAutoLauncherInfo;
        SceneManager.LoadScene(name);
    }

    public enum ConnectionType { UNET_LAN, UNET_LOBBY, }
    public static void Reconnect(ConnectionType  connection) {
        switch (connection)
        {
            case ConnectionType.UNET_LAN:
                ConnectToUNetLan();
                break;
            case ConnectionType.UNET_LOBBY:
                ConnectToUNetLobby();
                break;
            default:
                break;
        }
       
  
     }
    public static void Reconnect()
    {
        Reconnect(m_wantedConnection);
    }

        private static void ApplyAutoLauncherInfo(Scene arg0, Scene arg1)
    {
        NetworkManager manager = NetworkManager.singleton;

        //try
        //{
            //TODO Need to be settle 
            //A connection has already been set as ready. There can only be one.
//            UnityEngine.Networking.NetworkIdentity:UNetStaticUpdate()

            //if (!manager.isNetworkActive  )
            {
                Debug.Log("D-D"+ 
                    manager.matchName+ "  "
                    +(manager.client!=null? ""+manager.client.isConnected:"-"));
            //    manager.playerPrefab = GetUserPrefab();
            //    manager.matchName = GetRoomName();
            //    manager.onlineScene = Singleton.m_onlineScene;
            //    manager.offlineScene = Singleton.m_offlineScene;
            //    manager.networkPort = GetServerPort();
            }
        //}
        //catch (Exception e) { }
        SceneManager.activeSceneChanged -= ApplyAutoLauncherInfo;
        
    }

    public static void KillAllNetworkManager()
    {
        foreach (NetworkManager networkManager in GameObject.FindObjectsOfType<NetworkManager>())
        {
            networkManager.StopServer();
            networkManager.StopMatchMaker();
            networkManager.StopClient();
            networkManager.StopHost();
            GameObject. Destroy(networkManager.gameObject);
        }
    }
}
