﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Dropdown))]
public class DropdownPref : MonoBehaviour
{
    public string m_Id;
    public Dropdown m_target;
    // Use this for initialization
    private void Awake()
    {
        if (m_target == null)
            m_target = GetComponent<Dropdown>();
    }
    void Start()
    {
        m_target.value = PlayerPrefs.GetInt("" + m_Id);
    }

    private void OnDestroy()
    {
        PlayerPrefs.SetInt("" + m_Id, m_target.value);
    }

    public void Reset()
    {
        if (m_target == null)
            m_target = GetComponent<Dropdown>();
        m_Id = "" + Random.Range(0, int.MaxValue);
    }
}
