﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_InfoToNetwork : MonoBehaviour {

    public InputField m_userName;
    public InputField m_roomName;
    public Dropdown m_connectionType;
    public NetworkAutoLauncher.ConnectionType[] m_dropIndexToConnection =
        new NetworkAutoLauncher.ConnectionType[] {
            NetworkAutoLauncher.ConnectionType.UNET_LAN,
            NetworkAutoLauncher.ConnectionType.UNET_LOBBY
        };
    public Text m_timeLeftAutoConnect;
    public float m_timeToAutoConnect=3f;

    public void Connect() {
        NetworkAutoLauncher.SetUserName(m_userName.text);
        NetworkAutoLauncher.SetRoomName(m_roomName.text);

        NetworkAutoLauncher.ConnectionType type = NetworkAutoLauncher.ConnectionType.UNET_LAN;
        int index = m_connectionType.value; ;
        if (index == 0) type = NetworkAutoLauncher.ConnectionType.UNET_LAN;
        if (index == 1) type = NetworkAutoLauncher.ConnectionType.UNET_LOBBY;
        NetworkAutoLauncher.SetConnectionType(type);
        NetworkAutoLauncher.Reconnect();
    }
    public void Wait(float time) {
        m_timeToAutoConnect = time;
    }
    public void Wait() {
        Wait(30);
    }
    
	
	// Update is called once per frame
	void Update () {
        m_timeLeftAutoConnect.text = "" + ((int)m_timeToAutoConnect);
        if (!string.IsNullOrEmpty(m_userName.text) && !string.IsNullOrEmpty(m_roomName.text)) 
        {
                if (m_timeToAutoConnect > 0f)
                {
                    m_timeToAutoConnect -= Time.deltaTime;
                    if (m_timeToAutoConnect < 0f)
                    {
                        m_timeToAutoConnect = 0;
                        Connect();

                    }
                }
        }
	}
}
